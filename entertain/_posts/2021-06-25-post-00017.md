---
layout: post
title: "[미드추천] 평단과 관객을 사로잡은 넷플릭스·왓챠·아마존 드라마 10選"
toc: true
---

 

 평단의 반응도 좋았고 관객들도 열광했던 미드 10 작품을 꼽아보았습니다. 송두리째 근작들인데요. 상의물론 근작들만을 대상으로 하더라도 훌륭한 작품들은 상의물론 열 작품을 상회하기 왜냐하면 IMDb와 로튼 토마토 지수 이외 화제 개인의 취향도 반영되었습니다.
 

 넷플릭스 오리지널 시리즈, 아마존 오지지널 시리즈를 기본으로 하지만, HBO, BBC 등의 방송사에서 제작한 드라마를 전 세계에 독점적으로 스트리밍 하는 경우도 포함하였습니다. 유난히 지금껏 시각 제작을 하고 있지 않은 왓챠의 경우는 농군 이토 방송사가 제작했지만 국내에서는 왓챠가 독점적으로 스트리밍하고 있는 경우입니다.
 

## 넷플릭스 NETFLIX
 

### #다크 #DARK
 2017년부터 공개된 넷플릭스 최초의 독일 오리지널 시리즈. SF 미스테리 드라마로 오리지널 시리즈 중에서도 수작으로 꼽힌다. 독일의 소도시 빈덴에서 벌어진 두 아이의 실종사건을 중심으로 33년 간격의 미래와 과거를 오가며 사건의 진실을 파헤치는 이야기를 다룬다. 2020년 시즌3로 완결되었다. (IMDb 8.8)
 

 

### 

### #블랙미러 #BLACKMIRROR
 영국의 SF 풍자 앤솔러지 드라마(특정 테마를 기준으로 연속성이 없는 이야기들을 모은 작품). 시즌 2까지는 영국 지상파TV '채널 4'에서, 시즌 3부터는 넷플릭스 오리지널 시리즈로 선보이고 있다.전 시즌 미디어와 정보기술 발달의 부작용이라는 주제에 초점을 맞추고 있다. 화이트 크리스마스(White Christmas), 샌주니페로(San Junipero), 시스템의 연인(Hang The DJ), 닥치고 춤 춰라(Shut Up and Dance), 추락(Nosedive) 등의 에피소드가 좋은 평을 받은 바 있다. 총 5개의 시즌을 선보였다. (IMDb 8.8)
 

 

 

### #믿을수없는이야기 #UNBELIEVABLE
 2019년 9월 첫 공개된 넷플릭스 오리지널 시리즈. T. 크리스천 밀러와 켄 암스트롱의 퓰리처상 수상작 <더 마셜 프로젝트(The Marshall Project)>와 프로퍼블리카의 노트 〈믿을 복 없는 강간 이야기〉(An Unbelievable Story of Rape) 등 실지로 사건에서 영감을 얻어 워싱턴과 콜로라도에서 발생한 연쇄강간사건을 소재로 다룬다. 로튼 토마토 지수 최상 97%로 높은 완성도로 화제를 모았다. (IMDb 8.4)
 

 

### 

### 

### #마인드헌터 #MINDHUNTER
 2017년부터 넷플릭스에서 공개된 미국 죄범 드라마. 시즌 2까지 공개되었다. 영화 <세븐>, <파이트클럽>, <조디악>, <나를 찾아줘>의 감독자 '데이비드 핀처'와 헐리우드 탑배우 '샤를리즈 테론'이 제작하였다. #FBI 최초의 #프로파일러 존 E. 더글러스의 실지로 죄 기록서를 원작으로 1970년대 풍설 FBI 행동과학부 요원들이 당대 최악의 살인자들을 인터뷰하면서 '연쇄살인'과 '프로파일링'의 개념이 탄생하고 형성되어가는 과정을 리얼하게 그린다. 시즌 3 제작에 대한 요청이 쇄도하고 있으나 데이비드 핀처가 <러브, 데스 + 로봇> 제작을 벌써 하겠다고 밝혀 시즌 3 제작이 요원한 상황이다. (IMDb 8.6)
 

 

 

 

### #마스터오브제로 #MasterofNone
 2015년 첫 공개된 넷플릭스 오리지널 시리즈. 시즌 2까지 공개되었다. 미국 NBC 극 <팍스 앤 레크리에이션>으로 인기를 모은 인도계 코미디언이자 배우인 '아지즈 안사리', 동 드라마의 작가, 프로듀서로 에미상 후보에 올랐던 '알란 양', 배우이자 작가인 '리나 웨이스'가 작가, 감독, 배우로 나란히 참여한 작품. 아지즈 안사리와 리나 웨이스는 2017년 에미상에서 본 드라마로 코미디 각본상을 공동 수상하였다. 로튼토마토가 선정한 베스트 넷플릭스 시리즈(BEST NETFLIX SERIES)중 1위를 기록한 바 있다. (IMDb 8.3)
 

 

## 왓챠  WATCHA
 

### [넷플릭스](https://followtendency.cf/entertain/post-00002.html) #체르노빌 #CHERNOBYL
 HBO의 2019년 문제작 <체르노빌 CHERNOBYL>. 1986년 4월 발생한 체르노빌 원자력 발전소 사고와 유례없는 대규모 정화 작전, 이이 현장에 휘말린 사람들의 이야기를 5개 에피소드에 담았다. 2019년 에미상에서 작품상, 감독상, 각본상 등 10개 부문을 휩쓴 화제작으로 압도적인 몰입감과 다름없이 'What is the cost of lies? (거짓의 대가가 무엇일까?)'라는 묵직한 메시지를 전한다. (IMDb 9.4)
 

 

### 

### 

### #킬링이브 #KillingEve
 영국 BBC 아메리카에서 2018년 4월부터 방송 군속 드라마. 루크 젠닝스의 사연 <Codename Villanelle>을 원작으로 하며 <플리백 Fleabag>의 제작, 각본, 주연으로 유명한 '피비 월러브리지'가 제작, 각본에 참여하였다. 주연인 샌드라 오와 조디 코머가 골든 글로브, 에미상 등에서 다수의 여우주연상을 수상하였다. 시즌 3까지 공개되었다. (IMDb 8.3)
 

 

## 아마존 프라임 비디오  AMAZON PRIME VIDEO
 

### #플리백 #FLEABAG
 2019년 제71회 에미상에서 여우주연상, 각본상, 감독상, 연출상 4관왕을 차지한 BBC 분산 드라마. 전 세계에는 아마존프라임비디오 오리지널 시리즈로 공개되었다. 드라마 《킬링 이브》의 제작자, 각본가로 유명한 영국의 배우, 작가, 제작자인피비 월러브리지가 기획·각본·주연을 맡았다.극 중 주인공의 닉네임인 ‘fleabag’은 ‘행색이 초라하고 불쾌한 사람’ 또 ‘싸고 더러운 호텔’을 의미한다. (IMDb 8.7)
 

 

 

### #더보이즈 #TheBoys
 2019년 7월 첫 공개된 #아마존오리지널 시리즈. 명실공히 아마존 오리지널을 대표하는 시리즈로 좌상 잡았다. 2006년 풍 에니스가 다이너마이트에 연재한 만화 <더 보이즈>를 원작으로한다. 영화 <롱샷> <파인애플 익스프레스> <사고친 후에> 등으로 유명한 배우, 영화 제작자, 감독, 각본가인 세스 로건이 각본에 참여하였다. 기존 슈퍼히어로물과 도통 궤를 달리하는 슈퍼히어로 드라마로 미국 평단에서 호평을 받았다. 2020년 9월 시즌 2를 공개하였다. (IMDb 8.7)
 

 

### 

### 

### #마블러스미세스메이즐 THE MARVELOUS Mrs. MAISEL
 2017년 첫 공개된 아마존 프라임 비디오 오리지널 시리즈. 아마존 오리지널을 대표하는 시리즈 중도 하나다. 드라마 <길모어 걸스>의 에이미 셔먼 팔라디노가 각본을 맡아 시즌 3까지 제작되었다. 1950년대를 배경으로 모두가 선망하는 행복했던 결혼생활을 살던 주부가 스탠드업 코미디언으로 변신하게 되는 내용을 다룬다. 골든글로브, 에미상 등에서 작품상 및 여우주연상 등 다수의 상을 수상하였다. (IMDb 8.7)
 

