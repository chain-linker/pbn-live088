---
layout: post
title: "경기도 용인시 임장후기"
toc: true
---

 안녕하세요. 비류입니다.
 ​
 이번주는 용인-수원-안산-시흥-부천을 다녀와서 임장후기를 올립니다.
 용인시는 21개의 동으로 나눠서 후기를 남길텐데, 용인시의 크기가 썩 크기때문에 특별한 입지적인 특징이 없는곳은 빠르게 넘기는 경우가 많습니다.
 외곽이면서 입지가 좋지않은 처인구청부터 반시계방향으로 거저 돌면서 광교로 들어가는 루트를 세웠기때문에 처음엔 후기에 내용이 짧습니다.
 ​
  1. 권역분류 및 임장후기
  - 빨간원>파란원>검정원 순으로 급지가 낮아지며, 초록원은 대규모 재건축이 일어나는 곳임.
 ​
  1) 고림동
  - 특성 : 최외곽
  - 사진설명 : 1번 고지역/2번 고림2차양우내안에에듀퍼스트/3번 인근인프라/4번 분양권 경쟁률
  - 숙려 : 최외곽이며 이것을 지하철이 어느정도 헷지시켜줄 수명 있을까 하고 매매가격을 살펴보니, 에버랜드라인은 역세권이라는 영향이 크게 없어보입니다.
 오는길에 공사판이었고, 인근에 공사중인 단지가있어 이곳의 수요를 알아보기위해 청약경쟁률을 살펴보니 1순위 완판입니다. 윗권역보단 아랫권역의 인프라가 어서 되어있고 그래도 구도심 중심과 가깝습니다.
 ​
  2) 김량장동
  - 캐릭터 : 구도심상권
  - 응사 : 금요일 저녁 8시경 모습인데 술집과 귀퉁이 음식점을 제외한 가게문이 십중팔구 닫혀있습니다. 지나가는 행인 사이에서 '여긴 열렸다' 하며 식사를 하러 들어가는 모습을 봤습니다.
 평택역 옆집 구도심의 확장판이라는 생각이 들었으며, 그래도 위치 좋은곳에 신축아파트(용인드마크데시앙)이 분양하여 수요를 알아보기 위해 청약경쟁률을 살펴보니, 수요가 있습니다.
 고림동도 그렇고 용인시 외곽임에도 완판이 되는 모습을 볼 복수 있었습니다.
 ​
  3) 역북동
  - 특색 : 소규모 신도시느낌
  - 사진설명 : 1번 역북동 대로변/2번 롯데시네마, 의류가게 등등/3번 우미린센트럴파크 대가리 오피스텔/4번 소수학원
  - 분별 : 그래도 외곽으로 취급받는 이곳에선 대장역할을 하는 곳이라고 봅니다. 초등학교를 품고있는 우미린센트럴파크에게 1등을 주고싶고, 정문을 나오면 오피스텔에 딸린 상가가 예쁘게 지어져있습니다.
 아래로 내려갈수록 소수의 학원도 있습니다.
 ​
  4) 보라동
  - 속성 : 상권, 학원가 소수밀집
  - 계획 : 외곽이지만 상권과 학원이 밀집되어있는 보라동입니다. 학원의 느낌은 평택의 비전동과 유사합니다.
 ​
  5) 중동
  - 중동은 두 권역으로 나누겠습니다
 ​
  5-1) 중동a
  - 개성 : 대단지 아파트 밀집
  - 상계 : 지금까지 아파트가 송두리 구축이었다면 극히 신축느낌의 아파트가 밀집되어있는 중동a권역입니다.
 두산위브더제니스 대갈통 단지상가가 신축으로 무게 되어있습니다.
 ​
  5-2) 중동b
  - 고유성 : 자연환경, 학원가, 상권
  - 배의 : 중동a 권역보다 동백호수공원인근의 인프라가 한결 잘되어있었습니다.
 광주 효천지구느낌의 항아리상권이 있고, 영화관도 있으며 저녁에 예쁘게 분수쇼가 나오고 영화를 촬영하는 모습도 볼 핵심 있었습니다.
 ​
  6) 동백동
  - 성격 : 항아리상권
  - 안출 : 중동과 가까운 권역이면서 항아리상권으로 되어있고 동백호수공원 만큼의 규모는 아닙니다.
 ​
  7) 구갈동
  - 구갈동은 세권역으로 나누겠습니다.
 ​
  7-1) 구갈동a
  - 부성 : 요구 수부 상권, 재건축 밭
  - 속종 : 오산시청과 유사하지만 규모는 작다고 느꼈습니다. 상권, 소수학원, 위락 등 오산시청과 비슷합니다. 주변이 첫머리 구축아파트인것이 아쉬웠는데, 대개 모든 아파트가 재건축 예정입니다. 이곳은 수인분당선이 연결되어있는 기흥역이 있으므로 발전이되는 것 같고, AK플라자 인사 신축아파트와 더불어 금리 일대도 멋지게 변할 것 같습니다.
 ​
  7-2) 구갈동b
  - 성결 : 더블역세권, 신도시느낌
  - 소리 : 창원의 유니시티같이 수익 권역만 특별한 느낌이 있습니다. 특히 힐스테이트기흥은 수인분당선이 연결된 기흥역과 AK플라자를 끼고있으니 기흥구에서 대장을 차지하고 있습니다.
 ​
  7-3) 구갈동c
  - 특이성 : 강남대학교
  - 사모 : 나름의 학원, 상권이 갖춰져있으나 인근에 대학교가 있는데 대학생들이 누릴만한 상권이 없다는게 의아했습니다.
 ​
  8. 상갈동
  - 부성 : 베드타운
  - 견지 : 베드타운느낌의 구축아파트와 소수의 학원이 있지만 수인분당선이 연결되어있는 상갈역이 있습니다. 뚜렷한 특징을 가진 입지가 아니라 수인분당선이 깔려있음에도 매매가격에는 크게 영향을 받지 않는듯 합니다. 구갈동c와 평당가가 비슷합니다.
 ​
  9. 신갈동
  - 특질 : 구갈동a 인접
  - 소감 : 이곳은 도현현대아이파크의 평당가가 높아서 호갱노노를 찾아보니, 다만 분위기 엘레베이터로 대번에 역을 이용할 수 있는 모양입니다. 집안 현관에서부터 5분내로 승차까지 가능하다고 합니다.
 ​
  10) 언남동
  - 고유성 : 대다수 상권
  - 감상 : 역세권은 아니지만 상권형성이 곧바로 되어있어 장미마을삼성래미안2차만 높은 평당가를 가지는 모습입니다.
 ​
  11) 마북동
  - 캐릭터 : 플랫폼시티
  - 소갈딱지 : 냉큼 왼쪽 보정동에 플랫폼시티가 지어지고있으며, 그에 맞춰 아파트명에 플랫폼시티가 붙습니다. 현수막으로 아파트명이 변경되었다며 자축하는 모습을 봤습니다.
 ​
  12) 보정동
  - 속성 : 학원가, 상권, 의료시설
  - 지각 : 반드시 들어오는 순간 평촌 느낌이 날정도로 인프라가 되어있는데 가게의 종류를 보니 학원이 많긴하지만 평촌정도의 학원가는 아닙니다. 플랫폼시티가 완공되면 소천 수혜를 받는 권역이지 않을까 싶습니다.
 학관 현수막에 '서울대합격'이라는 단어도 매우 보입니다. 젊은층의 유동인구도 꽤 많았습니다.
 ​
  13) 죽전동
  - 개성 : 학원가, 상권, 의료시설
  - 기억 : 용인시에서 진학률1위인 용인대덕중학교가 있는곳입니다. 상권과 학원의 규모는 보정동보단 작습니다.
 ​
  14) 풍덕천동
  - 수지구청이 있는 풍덕천동은 크게 두권역으로 나누겠습니다.
 ​
  14-1) 풍덕천동a
  - 소질 : 재건축 다수, 역세권, 학원/상권 밀집
  - 견지 : 구청인근 치곤 유흥가가 무척 보이지 않았습니다. 바깥상권에 학원가 밀집, 안쪽상권에 식당과 술집이 있습니다. 게다가 이곳은 재건축밭으로, 한성아파트는 현대건설에서 재건축 확정이라며 플랭카드가 걸려있습니다.
 ​
  14-2) 풍덕천동b
  - 부성 : 요 학원가
  - 지의 : 하천이 하나 있어 경계를 나누는 느낌이 드는 곳입니다. 굳이 수지구청이 있는 학원가까지 범주 않더라도 학원을 이용할 수명 있도록 어느정도 인프라가 구성되어있습니다.
 ​
  15) 동천동
  - 동천동도 두권역으로 나누겠습니다.
 ​
  15-1) 동천동a
  - 기질 : 대단지
  - 사변 : 대전의 도안신도시같이 외곽에 위치하면서 대단지아파트가 밀집되어있습니다. 연식은 약 10년차입니다.
 ​
  15-2) 동천동b
  - 성식 : 역세권
  - 의견 : 지적편집도상 상권이라 되어있는 이곳은 유통단지로 이루어져 있습니다. 인근에 소소한 상권과 학원이 있습니다.
 신분당선이 깔려있는 동천역과 가깝지만 여건 입지는 슬쩍 아쉽습니다.
 ​
  16) 신봉동
  - 고유성 : 요 학원밀집, 상권
  - 신경 : 역과 너무나 떨어져있지만 소소하게 학원이 형성되어있고, 취중 신축인 수지스카이뷰푸르지오 옆에는 상권형성이 삭삭 되어있습니다.
 ​
  17) 성복동
  - 적성 : 역세권, 롯데몰
  - 사진설명 : 1번 성복역롯데캐슬/2번 롯데몰/3~5번 성복역 인프라/6~7번 성복역 정문 전도 상권
  - 의지 : 오는 등시 동탄역롯데캐슬이 생각났습니다.
  1) 동탄역은 SRT와 롯데몰을 끼고있으며 인근에 학원과 상권이 신도시느낌으로 쾌적하게 있는데 반해,
  2) 성복역 롯데캐슬은 신분당선을 끼고 롯데몰이 있습니다. 상권은 구도심느낌입니다.
 동탄역시범더샵센트럴시티와 성복역롯데캐슬골드타운/e편한세상수지 비교 악가 서로서로 같은 평당가로 흘러갑니다.
 ​
  18) 상현동
  - 상현동은 용인시라기보단 수원의 광교 생활권역으로 보면 좋을 것 같습니다. 두 권역으로 나누겠습니다.
 ​
  18-1) 상현동a
  - 성품 : 역세권, 학원/상권 밀집, 신도시느낌
  - 사진설명 : 1번 광교자이더클래스/2~4번 상현역 상권,학원
  - 목구멍소리 : 앞서 상현역을 중심으로 좌우대칭이 되듯 상권/학원/아파트가 형성되어 있습니다. 유흥가는 무진히 보이지않았고, 이자 권역에선 학교가 가깝고 브랜드가 있는 광교자이더클래스가 대장을 차지하고 있습니다.
 역시 성복역이 갖지못한 신도시느낌의 쾌적함이 이곳 상현동에는 있기에 연식이 7년 뒤쳐지더라도 비슷한 평당가흐름을 가지고 있습니다. (비슷한 입지인데 신도시와 구도심의 차이는 15%정도 두면 되는걸까요? 다음에 비슷한 입지를 발견한다면 검증해보기로..)
 ​
  18-2) 상현동b
  - 개성 : 구축밀집
  - 목 : 상권이 밀집되어있는 곳이라 의외라는 생각이 들었던 곳입니다. 이윤 권역의 대장은 신축이면서 브랜드가 있는 더샵수지포레아파트가 대장을 차지하고 있습니다.
 ​
  19) 용인플랫폼시티
  - 색채 : 용인플랫폼시티, GTX예정
  - 사려 : 사진과 나란히 현수막이 걸려있고 주변 카페가 있기에 궁금해서 커피한잔에 무려 5천원 거금을 지불하며 직원분께 여쭤봤습니다. GTX와 플랫폼이 들어오는데 보상이 그만 이루어지지 않는다고 시위를 한다고 합니다.
 뉴스에서만 보던 용인플랫폼시티를 직접와서 보니, 과천신도시(과천원도심 위)와 함께 채 개발예정이고 착공도 다리깽이 않았는데 매매가격이 뻥튀기되고 있다는 소문이 이해되지 않았습니다.
 ​
  20) 영덕동
  - 마음 : 수원시 생활권, 상권밀집
  - 짐작 : 이곳은 용인이라기보단 수원시 생활권이라 봐야할 것 같습니다. 다수의 상권과 소수학원이 밀집되어있는 특징이 있습니다.
 ​
  21) 하갈동
  - 품성 : 외곽
  - 관심 : 효성해링턴플레이스 앞뒷집 단지상가가 상천 [힐스테이트수원파크포레](https://inconclusivepart.com/life/post-00053.html) 되어있긴 도리어 너무너무 외곽에 혼자노는 느낌을 받았습니다.
 ​
  2. 프리미엄별 급지분류(수원 생활권 제외-상현동,영덕동,하갈동)
  1) 상권
  1등 : 성복동
  2등 : 풍덕천동
  3등 : 보정동
  4등 : 죽전동
 ​
  2) 학원가
  1등 : 풍덕천동
  2등 : 보정동
  3등 : 죽전동
 ​
  3) 미래변화정도
  1등 : 풍덕천동
  2등 : 구갈동
 ​
  3. 권역별 급지분류(수원시 생활권 제외)
  1군 : 성복동
  1.5군 : 풍덕천동
  1.7군 : 동천동
  2군 : 구갈동, 보정동
  2.5군 : 죽전동
  3군 : 중동, 신봉동
